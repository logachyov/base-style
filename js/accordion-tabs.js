(function ($, Drupal) {
  Drupal.behaviors.accordionTabs = {
    attach: function (context, settings) {
      let accordion = function () {

        function _setUpListeners() {
          var $accordion = $('.paragraph--type--faq-list');

          $accordion.once().on('click', '.block-field-blocknodefaqtitle', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $item = $this.closest('.paragraph--type--faq-list .field--name-field-reference > .field__item');
            var $items = $item.siblings();
            var $content = $item.find('.block-field-blocknodefaqbody');
            var $siblingsContent = $items.find('.block-field-blocknodefaqbody');

            if ($item.hasClass('is-open')) {
              $item.removeClass('is-open');
              $content.stop(true, true).slideUp(300);
            }
            else {
              $items.removeClass('is-open');
              $siblingsContent.stop(true, true).slideUp(300);
              $item.addClass('is-open');
              $content.stop(true, true).slideDown(300);
            }
          });
        }

        function init() {
          _setUpListeners();
        }

        return {
          init: init
        };
      }

      accordion().init();
    }
  };
})(jQuery, Drupal);