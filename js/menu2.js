(function ($, Drupal) {
  Drupal.behaviors.menuOpen = {
    attach: function (context, settings) {
      let menuElement = $('#header .menu .expanded');
      let defaultLink = $('#header .menu .expanded > a');
      let menuDrop = $('#header .menu .menu');

      defaultLink.on('click', function(e) {
        e.preventDefault();
      })

      menuElement.once().on('click', function(e) {
        let menuDr = $(this).find('.menu');

        if (!menuDr.is(':visible')) {
          menuElement.removeClass('open')
          menuDrop.slideUp(300);
        }

        $(this).toggleClass('open');
        menuDr.slideToggle(300);
      })

      $(document).mouseup(function (e){
        if (!menuElement.is(e.target) && menuElement.has(e.target).length === 0) {
          menuElement.removeClass('open')
          menuElement.find('.menu').slideUp(300);
        }
      });
    }
  };
})(jQuery, Drupal);