(function ($, Drupal) {
  Drupal.behaviors.anchorScroll = {
    attach: function (context, settings) {

      const anchorBar = document.querySelector('.section--anchor-navigation');
      if (!anchorBar) return;

      const anchorLinks = anchorBar.querySelectorAll('.view-anchor-navigation a');
      const anchorСontents = anchorBar.querySelectorAll('.view-anchor-navigation .view-content a');
      const anchorHead = anchorBar.querySelectorAll('.view-anchor-navigation .view-header a');

      const options = {
        activeClass: 'current',
        duration: 500,
        offset: -140,
      };

      const scrollTo = (target, options) => {
        const start = window.pageYOffset;

        const opts = {
          duration: options.duration,
          offset: options.offset || 0,
          callback: options.callback,
          easing: options.easing || _easeInOutQuad,
        };

        const distance =
          target === ''
            ? document.querySelector('body').getBoundingClientRect().top
            : opts.offset + document.querySelector(target).getBoundingClientRect().top;
        const duration = typeof opts.duration === 'function' ? opts.duration(distance) : opts.duration;
        let timeStart;
        let timeElapsed;

        requestAnimationFrame(time => {
          timeStart = time;
          _loop(time);
        });

        const _loop = time => {
          timeElapsed = time - timeStart;
          window.scrollTo(0, opts.easing(timeElapsed, start, distance, duration));
          if (timeElapsed < duration) requestAnimationFrame(_loop);
          else end();
        };

        const end = () => {
          window.scrollTo(0, start + distance);
          if (typeof opts.callback === 'function') opts.callback();
        };

        function _easeInOutQuad(t, b, c, d) {
          t /= d / 2;
          if (t < 1) return (c / 2) * t * t + b;
          t--;
          return (-c / 2) * (t * (t - 2) - 1) + b;
        }
      };

      const activeAnchors = offset => {
        const fromTop = window.scrollY;
        let flag = false;

        anchorСontents.forEach(link => {
          const section = link.hash !== '' ? document.querySelector(link.hash) : '';

          if (section) {
            const sectionTop = offset + section.offsetTop;
            if (sectionTop <= fromTop && sectionTop + section.offsetHeight > fromTop) {
              link.classList.add(options.activeClass);
            } else {
              link.classList.remove(options.activeClass);
            }
          }
        });

        anchorHead.forEach(link => {
          const section = link.hash !== '' ? document.querySelector(link.hash) : '';

          if (section) {
            const sectionTop = offset + section.offsetTop;
            if (sectionTop <= fromTop && sectionTop + section.offsetHeight > fromTop) {
              link.classList.add(options.activeClass);
            } else {
              link.classList.remove(options.activeClass);
            }
          }
        });
      };

      window.addEventListener('scroll', () => {
        let baseOffsetAnchor = -300;
        let offsetAnchor = baseOffsetAnchor;

        if (document.body.classList.contains('toolbar-fixed')) {
          offsetAnchor = baseOffsetAnchor + 50;

          if (
            document.body.classList.contains('toolbar-horizontal') &&
            document.body.classList.contains('toolbar-tray-open')
            ) {
              offsetAnchor = baseOffsetAnchor + 90;
          }
        }

        activeAnchors(offsetAnchor);
      });

      anchorLinks.forEach(link => {
        link.addEventListener('click', e => {
          e.preventDefault();
          let mql = window.matchMedia('all and (max-width: 767px)');

          let topOffset = 0;
          let headerTopOffset = -140;

          if (mql.matches === true ) {
            headerTopOffset = -130
          }

          if (document.body.classList.contains('toolbar-fixed')) {
            topOffset = -39;

            if (
              document.body.classList.contains('toolbar-horizontal') &&
              document.body.classList.contains('toolbar-tray-open')
            ) {
              topOffset = -78;
            }

            scrollTo(e.target.hash, { duration: options.duration, offset: headerTopOffset + topOffset });
          } else {
            scrollTo(e.target.hash, { duration: options.duration, offset: headerTopOffset });
          }
        });
      });
    }
  };
})(jQuery, Drupal);