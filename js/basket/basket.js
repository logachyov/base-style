(function ($, Drupal) {
    $(document).ready(function() {
        const mediaQuery = window.matchMedia('(max-width: 900px)');

        /*if (mediaQuery.matches) {
            if(document.querySelector('.views-view-table tbody')) {
                const tBodyBasket = document.querySelector('.views-view-table tbody');
                tBodyBasket.insertAdjacentHTML("beforeend", `<tr><td class="table-js-desc" colspan="2"></td><td class="table-js-btn-delete"></td></tr><tr class="table-tr-js-total"><td class="table-js-total" colspan="2"></td><td class="table-js-total-num"></td></tr>`)
                $('.views-view-table .details-wrapper').clone().appendTo('.table-js-desc');
                $('.views-view-table .delete-order-item').clone().appendTo('.table-js-btn-delete');

            }
        }*/

        const mobileBasket = document.querySelector('.path-cart form');

        if (mobileBasket) {
            mobileBasket.insertAdjacentHTML("afterbegin", `
            <div class="mobile-basket">
                <div class="mobile-basket__header">
                    <div class="header-item header-item__item"></div>
                    <div class="header-item header-item__price"></div>
                    <div class="header-item header-item__quantity"></div>
                </div>

                <div class="mobile-basket__content">
                    <div class="content-item content-img"></div>
                    <div class="content-item content-price"></div>
                    <div class="content-item content-quantity"></div>
                </div>

                <div class="mobile-basket__info">
                    <div class="info-desc"></div>
                    <div class="info-delete"></div>
                </div>

                <div class="mobile-basket__footer">
                    <div class="footer-total"></div>
                    <div class="footer-price"></div>
                </div>
            </div>`)

            document.querySelector('.header-item__item').innerHTML = document.querySelector('.path-cart form thead .views-field-nothing').innerHTML;
            document.querySelector('.header-item__price').innerHTML = document.querySelector('.path-cart form thead .views-field-nothing-1').innerHTML;
            document.querySelector('.header-item__quantity').innerHTML = document.querySelector('.path-cart form thead .views-field-edit-quantity').innerHTML;
            
            $('.path-cart form .image-wrapper img').clone().appendTo('.mobile-basket .content-img');// img
            document.querySelector('.mobile-basket .content-price').innerHTML = document.querySelector('.path-cart form .price-wrapper').innerHTML;
            document.querySelector('.mobile-basket .content-quantity').innerHTML = document.querySelector('.path-cart form .form-type-number').innerHTML;// input

            document.querySelector('.mobile-basket .info-desc').innerHTML = document.querySelector('.path-cart form .details-wrapper').innerHTML;
            $('.path-cart form .views-field-remove-button input').clone().appendTo('.mobile-basket .info-delete');// btn

            document.querySelector('.mobile-basket .footer-total').innerHTML = document.querySelector('.views-view-table thead .views-field-total-price__number').innerHTML;
            document.querySelector('.mobile-basket .footer-price').innerHTML = document.querySelector('.views-view-table tbody .views-field-total-price__number').innerHTML;
        }

    });
})(jQuery, Drupal);
