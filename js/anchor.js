export default () => {
  const anchorBar = document.querySelector('.anchor-navigation');
  if (!anchorBar) return;
  if (!anchorBar.querySelector('.view-anchor-navigation')) {
    anchorBar.style.display = 'none';
    return;
  }
  const header = document.querySelector('.header');
  const anchorLinks = anchorBar.querySelectorAll('.view-anchor-navigation a');
  const anchorСontents = anchorBar.querySelectorAll('.view-anchor-navigation .view-content a');
  const anchorHead = anchorBar.querySelector('.view-anchor-navigation .view-header a');
  const options = {
    stickyClass: 'sticky',
    activeClass: 'current',
    duration: 1500,
    offset: -100,
  };
  const scrollTo = (target, options) => {
    const start = window.pageYOffset;
    const opts = {
      duration: options.duration,
      offset: options.offset || 0,
      callback: options.callback,
      easing: options.easing || _easeInOutQuad,
    };
    const distance =
      target === ''
        ? document.querySelector('body').getBoundingClientRect().top
        : opts.offset + document.querySelector(target).getBoundingClientRect().top;
    const duration = typeof opts.duration === 'function' ? opts.duration(distance) : opts.duration;
    let timeStart;
    let timeElapsed;
    requestAnimationFrame(time => {
      timeStart = time;
      _loop(time);
    });
    const _loop = time => {
      timeElapsed = time - timeStart;
      window.scrollTo(0, opts.easing(timeElapsed, start, distance, duration));
      if (timeElapsed < duration) requestAnimationFrame(_loop);
      else end();
    };
    const end = () => {
      window.scrollTo(0, start + distance);
      if (typeof opts.callback === 'function') opts.callback();
    };
    function _easeInOutQuad(t, b, c, d) {
      t /= d / 2;
      if (t < 1) return (c / 2) * t * t + b;
      t--;
      return (-c / 2) * (t * (t - 2) - 1) + b;
    }
  };
  const activeAnchors = offset => {
    const fromTop = window.scrollY;
    let flag = false;
    anchorСontents.forEach(link => {
      const section = link.hash !== '' ? document.querySelector(link.hash) : '';
      if (section) {
        const sectionTop = offset + section.offsetTop;
        if (sectionTop <= fromTop && sectionTop + section.offsetHeight > fromTop) {
          link.classList.add(options.activeClass);
        } else {
          link.classList.remove(options.activeClass);
        }
      }
    });
    anchorСontents.forEach(link => {
      if (link.classList.contains(options.activeClass)) {
        flag = true;
      }
    });
    if (flag === true) {
      anchorHead.classList.remove(options.activeClass);
    } else {
      anchorHead.classList.add(options.activeClass);
    }
  };
  const sticky = (anchorBar, offset) => {
    anchorHead.classList.add(options.activeClass);
    window.addEventListener('scroll', () => {
      activeAnchors(options.offset);
      const currentScroll = window.pageYOffset;
      const anchorPos = anchorBar.getBoundingClientRect().top;
      let topOffset = header.getBoundingClientRect().height;
      if (document.body.classList.contains('toolbar-fixed')) {
        topOffset = header.getBoundingClientRect().height + 39;
        if (
          document.body.classList.contains('toolbar-horizontal') &&
          document.body.classList.contains('toolbar-tray-open')
        ) {
          topOffset = header.getBoundingClientRect().height + 79;
        }
      }
      if (header.classList.contains('sticky') && !header.classList.contains('scroll-down')) {
        anchorBar.style.top = `${topOffset}px`;
      } else {
        anchorBar.style.top = null;
      }
      if (currentScroll > anchorPos + offset) {
        anchorBar.classList.add(options.stickyClass);
      } else {
        anchorBar.classList.remove(options.stickyClass);
      }
    });
  };
  sticky(anchorBar, 50);
  anchorLinks.forEach(link => {
    link.addEventListener('click', e => {
      e.preventDefault();
      scrollTo(e.target.hash, { duration: options.duration, offset: options.offset });
    });
  });
  const anchorInnerBlock = anchorBar.querySelectorAll('.layout__region ')[0];
  anchorInnerBlock.addEventListener('wheel', function(event) {
    if (anchorInnerBlock.scrollLeft > 50) {
      if (!anchorBar.classList.contains('left-shadow')) anchorBar.classList.add('left-shadow');
    } else if (anchorInnerBlock.scrollLeft <= 50) {
      if (anchorBar.classList.contains('left-shadow')) anchorBar.classList.remove('left-shadow');
    }
    let modifier = 0;
    if (event.deltaMode == event.DOM_DELTA_PIXEL) {
      modifier = 1;
    } else if (event.deltaMode == event.DOM_DELTA_LINE) {
      modifier = parseInt(getComputedStyle(this).lineHeight);
    } else if (event.deltaMode == event.DOM_DELTA_PAGE) {
      modifier = this.clientHeight;
    }
    if (event.deltaY != 0) {
      this.scrollLeft += modifier * event.deltaY;
      event.preventDefault();
    }
  });
};
