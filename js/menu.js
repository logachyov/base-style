const menuBtn = document.querySelector('.hamburger-button');
const menuContent = document.querySelector('header .menu--main');

menuBtn.addEventListener('click', () => {
  menuBtn.classList.toggle('is-open');
  menuContent.classList.toggle('is-open');
})