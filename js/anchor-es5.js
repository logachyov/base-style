(function ($, Drupal) {
    Drupal.behaviors.anchorScrollAuthor = {
        attach: function (context, settings) {

            var anchorBar = document.querySelector('.content-without-author');
            if (!anchorBar) return;

            var anchorLinks = anchorBar.querySelectorAll('.field--name-dynamic-block-fieldnode-header .author-name a');

            var options = {
                activeClass: 'current',
                duration: 300,
                offset: -201,
            };

            var scrollTo = function (target, options) {
                var start = window.pageYOffset;

                var opts = {
                    duration: options.duration,
                    offset: options.offset || 0,
                    callback: options.callback,
                    easing: options.easing || _easeInOutQuad,
                };

                var distance =
                    target === ''
                    ? document.querySelector('body').getBoundingClientRect().top
                    : opts.offset + document.querySelector(target).getBoundingClientRect().top;
                var duration = typeof opts.duration === 'function' ? opts.duration(distance) : opts.duration;
                var timeStart;
                var timeElapsed;

                requestAnimationFrame(function(time) {
                    timeStart = time;
                    _loop(time);
                });

                var _loop = function(time) {
                    timeElapsed = time - timeStart;
                    window.scrollTo(0, opts.easing(timeElapsed, start, distance, duration));
                    if (timeElapsed < duration) requestAnimationFrame(_loop);
                    else end();
                };

                var end = function() {
                    window.scrollTo(0, start + distance);
                    if (typeof opts.callback === 'function') opts.callback();
                };

                function _easeInOutQuad(t, b, c, d) {
                    t /= d / 2;
                    if (t < 1) return (c / 2) * t * t + b;
                    t--;
                    return (-c / 2) * (t * (t - 2) - 1) + b;
                }
            };

            for(var i = 0; i < anchorLinks.length; i++) {
                anchorLinks[i].addEventListener('click', function(e) {
                    e.preventDefault();

                    var topOffset = 0;
                    var headerTopOffset = -221;

                    if (window.matchMedia("(max-width: 1540px)").matches) {
                        headerTopOffset = -291;
                    } else if (window.matchMedia("(max-width: 992px)").matches) {
                        headerTopOffset = -221;
                    }

                    if (document.body.classList.contains('toolbar-fixed')) {
                        topOffset = -39;

                        if (
                            document.body.classList.contains('toolbar-horizontal') &&
                            document.body.classList.contains('toolbar-tray-open')
                        ) {
                            topOffset = -78;
                        }

                        scrollTo(e.target.hash, { duration: options.duration, offset: headerTopOffset + topOffset });
                    } else {
                        scrollTo(e.target.hash, { duration: options.duration, offset: headerTopOffset });
                    }
                });
            }
        }
    };
})(jQuery, Drupal);

