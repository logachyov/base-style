(function ($, Drupal) {
  Drupal.behaviors.accordionDocuments = {
    attach: function (context, settings) {
      let mql = window.matchMedia('all and (max-width: 600px)');
      let documentationBtn = $('.paragraph--type--chapter-product.chapter-collapsible .field--name-field-title');
      let documentationContent = $('.paragraph--type--documents');
      let documentationContainer = $('.paragraph--type--chapter-product.chapter-collapsible');

      documentationBtn.once().on('click', function () {
        if (mql.matches === true ) {
          documentationContainer.toggleClass('open');
          documentationContent.slideToggle(300);
        } else if (mql.matches === false ) {
          return;
        }
      })
    }
  };
})(jQuery, Drupal);