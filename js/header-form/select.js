(function ($, Drupal) {
    Drupal.behaviors.selectForm = {
        attach: function (context, settings) {

            $('header select').once('headerselect').each(function() {
                const _this = $(this),
                    selectOption = _this.find('option'),
                    selectOptionLength = selectOption.length,
                    selectedOption = selectOption.filter(':selected'),
                    duration = 450; // длительность анимации

                _this.hide();
                _this.wrap('<div class="select"></div>');
                $('<div>', {
                    class: 'new-select',
                    text: _this.children('option:selected').text()
                }).insertAfter(_this);

                const selectHead = _this.next('.new-select');
                $('<div>', {
                    class: 'new-select__list'
                }).insertAfter(selectHead);

                const selectList = selectHead.next('.new-select__list');
                for (let i = 1; i < selectOptionLength; i++) {
                    $('<div>', {
                        class: 'new-select__item',
                        html: $('<span>', {
                            text: selectOption.eq(i).text()
                        })
                    })
                    .attr('data-value', selectOption.eq(i).val())
                    .appendTo(selectList);
                }

                const btnFormSea = $('.form-item-room')

                const selectItem = selectList.find('.new-select__item');

                btnFormSea.once().on('click', function() {
                    if ( !selectHead.hasClass('on') ) {
                        selectHead.addClass('on');
                        selectList.slideDown(duration);

                        selectItem.on('click', function() {
                            let chooseItem = $(this).data('value');

                            $('select').val(chooseItem).attr('selected', 'selected');
                            selectHead.text( $(this).find('span').text() );

                            selectList.slideUp(duration);
                            selectHead.removeClass('on');
                        });

                    } else {
                        selectHead.removeClass('on');
                        selectList.slideUp(duration);
                    }
                });

                $('body').on( 'click', function(e) {
                    if ($(e.target).closest(btnFormSea).length)
                        return;

                        selectHead.removeClass('on');
                        selectList.slideUp(duration);
                });
            });
        }
    };
})(jQuery, Drupal);